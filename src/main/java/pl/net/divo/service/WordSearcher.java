package pl.net.divo.service;

import lombok.AllArgsConstructor;
import pl.net.divo.util.WordsFilter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@AllArgsConstructor
public class WordSearcher {
    private Path dir;

    public List<Path> searchWordsRecursiveInDirectory(List<String> words) {
        List<Path> filesWithWords = new ArrayList<>();

        try (Stream<Path> stream = Files.walk(dir)) {
            stream.filter(Files::isRegularFile)
                    .forEach(f -> getFoundedFilesInPath(f, words).map(filesWithWords::add));
        } catch (Exception e) {
            return filesWithWords;
        }

        return filesWithWords;
    }

    private Optional<Path> getFoundedFilesInPath(Path path, List<String> words) {
        WordsFilter wordsFilter = new WordsFilter(words);

        try {
            return Files.lines(path)
                    .filter(wordsFilter::containsWord)
                    .findFirst()
                    .map(s -> path);
        } catch (IOException e) {
            return Optional.empty();
        }
    }
}
