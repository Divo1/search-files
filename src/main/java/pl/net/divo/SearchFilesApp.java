package pl.net.divo;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import pl.net.divo.configuration.SearchFileConfiguration;
import pl.net.divo.resources.SearchFilesFromQueryResource;
import pl.net.divo.service.WordSearcher;

import java.nio.file.Paths;

public class SearchFilesApp extends Application<SearchFileConfiguration> {
    public static void main(String[] args) throws Exception {
        new SearchFilesApp().run(args);
    }

    @Override
    public String getName() {
        return "search-files";
    }

    @Override
    public void initialize(Bootstrap<SearchFileConfiguration> bootstrap) { }

    @Override
    public void run(SearchFileConfiguration configuration,
                    Environment environment) {
        final WordSearcher wordSearcher = new WordSearcher(Paths.get(configuration.getSearchedPath()));

        final SearchFilesFromQueryResource resource = new SearchFilesFromQueryResource(wordSearcher);

        environment.jersey().register(resource);
    }

}