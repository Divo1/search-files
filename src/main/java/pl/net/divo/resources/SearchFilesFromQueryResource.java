package pl.net.divo.resources;

import com.codahale.metrics.annotation.Timed;
import lombok.AllArgsConstructor;
import pl.net.divo.model.Search;
import pl.net.divo.service.WordSearcher;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/files")
@Produces(MediaType.APPLICATION_JSON)
@AllArgsConstructor
public class SearchFilesFromQueryResource {
    private final WordSearcher wordSearcher;

    @GET
    @Timed
    public Search sayHello(@QueryParam("word-list") List<String> words) {
        return new Search(words, wordSearcher.searchWordsRecursiveInDirectory(words));
    }
}