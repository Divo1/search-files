package pl.net.divo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@Setter
@AllArgsConstructor
@Getter
public class Search {
    private List<String> searched = new ArrayList<>();
    private List<Path> filesPaths = new ArrayList<>();
}
