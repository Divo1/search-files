package pl.net.divo.util;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class WordsFilter {
    private List<String> words;

    public boolean containsWord(String line) {
        for (String k : words) {
            if (!line.toLowerCase().contains(k.toLowerCase())) {
                return false;
            }
        }
        return true;
    }
}
