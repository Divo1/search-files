package pl.net.divo.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class SearchFileConfiguration extends Configuration {

    @NotEmpty
    @JsonProperty
    private String searchedPath;
}
