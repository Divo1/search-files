package pl.net.divo;

import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import pl.net.divo.configuration.SearchFileConfiguration;
import pl.net.divo.model.Search;

import javax.ws.rs.client.Client;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AppIntegrationTest {
    @ClassRule
    public static final DropwizardAppRule<SearchFileConfiguration> RULE =
            new DropwizardAppRule<>(
                    SearchFilesApp.class,
                    ResourceHelpers.resourceFilePath("test.yml"));

    @Test
    public void shouldReturnEmptyFiles() throws IOException {
        Client client = RULE.client();
        List<String> words = Arrays.asList("test", "client");

        Search search = client.target("http://localhost:" + RULE.getLocalPort() + "/files")
                .queryParam("word-list", words.toArray())
                .request()
                .get(Search.class);

        Assert.assertEquals(0, search.getFilesPaths().size());
    }

    @Test
    public void shouldReturnOneFile() throws IOException {
        Client client = RULE.client();
        List<String> words = Arrays.asList("nunc", "lorem");

        Search search = client.target("http://localhost:" + RULE.getLocalPort() + "/files")
                .queryParam("word-list", words.toArray())
                .request()
                .get(Search.class);

        Assert.assertEquals(1, search.getFilesPaths().size());
    }

    @Test
    public void shouldReturnTwoFile() throws IOException {
        Client client = RULE.client();
        List<String> words = Collections.singletonList("lorem");

        Search search = client.target("http://localhost:" + RULE.getLocalPort() + "/files")
                .queryParam("word-list", words.toArray())
                .request()
                .get(Search.class);

        Assert.assertEquals(2, search.getFilesPaths().size());
    }
}
